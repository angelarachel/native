$( document ).ready(function() {
    const common = new CommonFunction();

    $.ajax({
        url: '/security-api/api/dtr/readByDateToday.php',
        type: 'POST',
        data: '',
        dataType: 'JSON',
        cache: false,
        success: (data) => {
            var dtrRowHtml = '';
            $.each(data.data, (key, dtrDetails) => {
                dtrRowHtml += common.rowInputDisplay(dtrDetails)
            });

            $('.js-dtr-rows').append(dtrRowHtml);
        },
        error: (error) => {

        }
    });

    $(document).on('click', '.js-dtr-edit', function() {
        common.showInput($(this));
    });

    $(document).on('click', '.js-dtr-save', function() {
        var inputValues = $(this).closest('tr').find(':input');
        var dtrDetails = common.parseInputValues(inputValues);
        var dtrRowHtmlAdded = common.rowInputDisplay(dtrDetails);
        common.hideInput($(this));
        $(this).closest('tr').prev().remove();
        $(this).closest('tr').before(dtrRowHtmlAdded);

        var data = $(inputValues).serialize();

        $.ajax({
            url: '/security-api/api/dtr/update.php',
            type: 'POST',
            data: data,
            dataType: 'JSON',
            cache: false,
            success: (data) => {

            },
            error: (error) => {

            }
        });
    });

    $(document).on('click', '.js-dtr-time-out', function() {
        var dtrId = $(this).closest('tr').next().find('.js-dtr-id:input').val();

        if(dtrId == undefined){
            dtrId = $(this).closest('tr').find('.js-dtr-id:input').val();
        }

        $.ajax({
            url: '/security-api/api/dtr/timeOut.php',
            type: 'POST',
            data: {'dtrId': dtrId},
            dataType: 'JSON',
            ContentType: 'application/json',
            cache: false,
            success: (data) => {
                console.log(data);
                $(this).closest('tr').next('tr').find('.js-time-out').html(data.result);
                $(this).closest('tr').next('tr').find('.js-time-out-val:input').val(data.result);
                $(this).closest('tr').find('.js-time-out').html(data.result);
            },
            error: (error) => {

            }
        });

    });

    $(document).on('click', '.js-dtr-add-row', function() {
        $(this).closest('tr').next().find(':input').val('');
        common.showInput($(this));

    });

    $(document).on('click', '.js-dtr-create-row', function() {
        var inputValues = $(this).closest('tr').find(':input');
        var dtrDetails = common.parseInputValues(inputValues);
        var dtrRowHtmlAdded = common.rowInputDisplay(dtrDetails);

        $('.js-dtr-rows').append(dtrRowHtmlAdded);

        common.hideInput($(this));

        var data = $(inputValues).serialize();

        $.ajax({
            url: '/security-api/api/dtr/create.php',
            type: 'POST',
            data: data,
            dataType: 'JSON',
            cache: false,
            success: (data) => {
                $('.js-dtr-id:last').val(data.result.dtrId);
                $('.js-dtr-time-in:last').html(data.result.timeIn);
                $('.js-dtr-time-in-display:last').html(data.result.timeIn);
                $('.js-dtr-time-in-val:last').val(data.result.timeIn);
            },
            error: (error) => {

            }
        });
    });

    $(document).on('click', '.js-dtr-cancel-create', function() {
        common.hideInput($(this));
    });

    $(document).on('click', '.js-dtr-cancel-save', function() {
        common.hideInput($(this));
        common.originalInput($(this));
    });
});

class CommonFunction {

    parseInputValues(inputValues){
        var dtrDetails = {};
        $.each(inputValues, (key, input) => {
            input = $(input).val();
            switch(key){
                case 0:
                    dtrDetails.dtrId = input;
                    break;
                case 1:
                    dtrDetails.givenName = input;
                    break;
                case 2:
                    dtrDetails.lastName = input;
                    break;
                case 3:
                    dtrDetails.purpose = input;
                    break;
                case 4:
                    dtrDetails.timeIn = input;
                    break;
                case 5:
                    dtrDetails.timeOut = input;
                    break;
            }
        });

        return dtrDetails;
    }

    showInput($this) {
        $this.closest('tr').next().addClass('show');
        $this.closest('tr').next().removeClass('hide');
        $this.closest('tr').removeClass('show');
        $this.closest('tr').addClass('hide');
    }

    hideInput($this){
        $this.closest('tr').prev().removeClass('hide');
        $this.closest('tr').prev().addClass('show');
        $this.closest('tr').removeClass('show');
        $this.closest('tr').addClass('hide');
    }

    originalInput($this){
        $.each($this.closest('tr').find(':input'), (key, dieselDetails) => {
            var attr = $this.closest('tr').find(':input').eq(key).attr('data-original');
            if (typeof attr !== typeof undefined && attr !== false) {
                $this.closest('tr').find(':input').eq(key).val(attr);
            }
        });
    }
    rowInputDisplay(dtrDetails)
    {
        var dtrRowHtml = '';
        var dtrId = '';
        var editLink = '<a href="javascript:void(0)" class="js-dtr-edit"> Edit </a>';
        var saveLink = '<a href="javascript:void(0)" class="js-dtr-save"> Save </a>';
        var timeIn = dtrDetails.timeIn == '' || dtrDetails.timeIn == undefined ? '' : dtrDetails.timeIn;
        var timeOut = dtrDetails.timeOut == ''  || dtrDetails.timeOut == undefined ? '' : dtrDetails.timeOut;
        var timeOutLink = timeOut == '' ? '<a href="javascript:void(0)" class="js-dtr-time-out"> Time Out </a>' : dtrDetails.timeOut;
        var cancelLink = '<a href="javascript:void(0)" class="js-dtr-cancel-save"> Cancel </a>';

        dtrRowHtml += '<tr class="js-row-display">';
        dtrRowHtml += '<td>' + dtrDetails.givenName + '</td>';
        dtrRowHtml += '<td>' + dtrDetails.lastName + '</td>';
        dtrRowHtml += '<td>' + dtrDetails.purpose + '</td>';
        dtrRowHtml += '<td class="js-dtr-time-in-display">' + timeIn + '</td>';
        dtrRowHtml += '<td> <span class="js-time-out">' + timeOutLink + '</span></td>';
        dtrRowHtml += '<td>' + editLink + '</td>';
        dtrRowHtml += '</tr>';

        dtrRowHtml += '<tr class="js-row-input hide">';
        if(dtrDetails.dtrId != undefined){
            dtrId = dtrDetails.dtrId;
        }
        dtrRowHtml += '<input type="hidden" class="js-dtr-id" value="'+dtrId+'" name="dtr_id">';
        dtrRowHtml += '<td>';
        dtrRowHtml += '<input value="'+dtrDetails.givenName+'" name="given_name">';
        dtrRowHtml += '</td>';
        dtrRowHtml += '<td>';
        dtrRowHtml += '<input value="'+dtrDetails.lastName+'" name="last_name">';
        dtrRowHtml += '</td>';
        dtrRowHtml += '<td>';
        dtrRowHtml += '<input value="'+dtrDetails.purpose+'" name="purpose">';
        dtrRowHtml += '</td>';
        dtrRowHtml += '<td> <span class="js-dtr-time-in">' + timeIn + '</span><input type="hidden" class="js-dtr-time-in-val" value="'+timeIn+'" name="time_in"> </td>';
        dtrRowHtml += '<td><span class="js-time-out">';
        dtrRowHtml += timeOutLink  + '</span> <input class="js-time-out-val" type="hidden" value="' +timeOut +'" name="time_out">';
        dtrRowHtml += '</td>';
        dtrRowHtml += '<td>' + saveLink  + ' &nbsp; &nbsp; ' + cancelLink +'</td>';
        dtrRowHtml += '</tr>';

        return dtrRowHtml;
    }
}
