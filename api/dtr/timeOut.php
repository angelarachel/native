<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

include_once '../../config/Database.php';
include_once '../../model/Dtr.php';

$database = new Database();
$db = $database->connect();

$dtr = new Dtr($db);
$data = json_decode(file_get_contents("php://input"), true);

$dtr->dtrId = $_POST['dtrId'] ?? $data['dtrId'];
date_default_timezone_set("Asia/Manila");
$dtr->timeOut = date("H:i:s");

if($dtr->timeOut()) {
    echo json_encode(
        ['result' => date ('h:i a', strtotime($dtr->timeOut))]
    );
} else {
    echo json_encode(
        ['message' => 'Error']
    );
}
