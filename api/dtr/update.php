<?php
// Headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization,X-Requested-With');

include_once '../../config/Database.php';
include_once '../../model/Dtr.php';

$database = new Database();
$db = $database->connect();
$dtr = new Dtr($db);
$data = json_decode(file_get_contents("php://input"), true);

$dtr->givenName = $_POST['given_name'] ?? $data['given_name'];
$dtr->lastName = $_POST['last_name'] ?? $data['last_name'];
$dtr->purpose = $_POST['purpose'] ?? $data['purpose'];
$dtr->dtrId = $_POST['dtr_id'] ?? $data['dtrId'];

// Update post
if($dtr->update()) {
    echo json_encode(
        ['result' => ['dtr_id' => $dtr->dtrId,
                     'given_name' => $dtr->givenName,
                     'last_name' => $dtr->lastName ,
                     'purpose' => $dtr->purpose]
    ]);
} else {
    echo json_encode(
        ['message' => 'Error']
    );
}
