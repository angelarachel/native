<?php

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../model/Dtr.php';

$database = new Database();
$databaseConnection = $database->connect();
$dtr = new Dtr($databaseConnection);
$result = $dtr->read();
$resultArray = [];
$resultArray['data'] = [];

while($row = $result->fetch(PDO::FETCH_ASSOC)){
   extract($row);

   $dtrItem = [
     'dtrId' => $row['dtr_id'],
     'givenName' => $row['given_name'],
     'lastName' => $row['last_name'],
     'timeIn' => date ('h:i a', strtotime($row['time_in'])),
     'timeOut'=> date ('h:i a', strtotime($row['time_out'])),
     'purpose'=> $row['purpose'],
   ];

    $resultArray['data'][] = $dtrItem;
}

echo json_encode($resultArray);