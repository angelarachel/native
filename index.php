<?php



?>
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<script src="js/dtr.js"></script>
<style>
    .hide {
        display:none;
    }

    .tb-dtr{
        width:100%;
        border: 1px solid #adadad;
        margin-top:20px;
    }

    .tb-dtr td {
        border: 1px solid #adadad;
        padding:5px 10px;
    }
    .tb-dtr th{
        background-color: #8bcdec;
        padding:10px 10px;
        cursor: pointer;
    }
    .tb-dtr th:first-child, th:nth-child(2) {
        width:15%;
    }

    .tb-dtr th:nth-child(3){
        width:30%;
    }
    .tb-dtr th:nth-child(4), th:nth-child(5) {
        width:10%;
    }

    .tb-dtr input {
        width:100%;
    }

</style>
<script>

    function myFunction() {
        var input, filter, table, tr, td, i, td1;
        input = document.getElementById("myInput");
        filter = input.value.toUpperCase();
        table = document.getElementById("myTable");
        tr = table.getElementsByTagName("tr");
        for (i = 2; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            td1 = tr[i].getElementsByTagName("td")[1];
            if (td || td1) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1 ||
                    td1.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }

        }
    }

    function sortTable(n) {
        var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        table = document.getElementById("myTable");
        switching = true;
        //Set the sorting direction to ascending:
        dir = "asc";
        /*Make a loop that will continue until
        no switching has been done:*/
        while (switching) {
            //start by saying: no switching is done:
            switching = false;
            rows = table.rows;
            /*Loop through all table rows (except the
            first, which contains table headers):*/
            for (i = 2; i < (rows.length - 1); i++) {
                //start by saying there should be no switching:
                shouldSwitch = false;
                /*Get the two elements you want to compare,
                one from current row and one from the next:*/
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                /*check if the two rows should switch place,
                based on the direction, asc or desc:*/
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch= true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        //if so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                /*If a switch has been marked, make the switch
                and mark that a switch has been done:*/
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                //Each time a switch is done, increase this count by 1:
                switchcount ++;
            } else {
                /*If no switching has been done AND the direction is "asc",
                set the direction to "desc" and run the while loop again.*/
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }
    }
</script>
<div id="dtr-table">

    <h2> <?php echo "Today is " . date("M d Y (l)") . "<br>"; ?> </h2>
    <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Search" >

</div>
<table class="tb-dtr" id="myTable">
    <thead>
    <tr>
        <th onclick="sortTable(0)">First Name</th>
        <th onclick="sortTable(1)">Last Name</th>
        <th onclick="sortTable(2)">Purpose</th>
        <th>Time In</th>
        <th>Time Out</th>
        <th>Action</th>
    </tr>
    <tr class="js-dtr-add">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><a href="javascript:void(0)" class="js-dtr-add-row"> Add </a></td>
    </tr>

    <tr class="js-dtr-create hide">
        <input type="hidden">
        <td><input name="given_name"></td>
        <td><input name="last_name"></td>
        <td><input name="purpose"></td>
        <td></td>
        <td></td>
        <td><a href="javascript:void(0)"  class="js-dtr-create-row"> Create </a>
            &nbsp; &nbsp; <a href="javascript:void(0)" class="js-dtr-cancel-create"> Cancel </a></td>

    </tr>
    </thead>

    <tbody class="js-dtr-rows">


    </tbody>
</table>

