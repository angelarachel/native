<?php
/**
 * Created by PhpStorm.
 * User: iAm
 * Date: 20/01/2019
 * Time: 7:45 PM
 */

class Database
{
    private $host = 'localhost';
    private $databaseName = 'security_logs';
    private $password = '';
    private $username = 'root';
    private $connection;

    public function connect()
    {
        $this->connection = null;

        try {
            $this->connection = new PDO('mysql:host ='.$this->host.';dbname='.$this->databaseName, $this->username, $this->password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }catch (PDOException $exception){
            echo 'Connection Error '. $exception->getMessage();
        }

        return $this->connection;
    }
}