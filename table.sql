CREATE TABLE `dtr` (
  `dtr_id` int(11) NOT NULL,
  `given_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `purpose` varchar(200) NOT NULL,
  `time_in` time NOT NULL,
  `time_out` time DEFAULT NULL,
  `date` date NOT NULL,
  `signature` varchar(25) DEFAULT NULL,
  `modified_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `dtr`
  ADD PRIMARY KEY (`dtr_id`);

ALTER TABLE `dtr`
  MODIFY `dtr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

