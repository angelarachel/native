<?php
/**
 * Created by PhpStorm.
 * User: iAm
 * Date: 20/01/2019
 * Time: 8:09 PM
 */

class Dtr
{
    private $connection;
    private $tableName = 'dtr';

    public $dtrId;
    public $givenName;
    public $lastName;
    public $purpose;
    public $timeIn;
    public $timeOut;
    public $modified;
    public $date;

    public function __construct($db)
    {
        $this->connection = $db;
    }

    public function read()
    {
        $dtr = 'SELECT * FROM '. $this->tableName;

        $statement = $this->connection->prepare($dtr);

        $statement->execute();

        return $statement;
    }

    public function create()
    {
        date_default_timezone_set("Asia/Manila");
        $query = 'INSERT INTO ' . $this->tableName . ' SET given_name = :givenName, last_name = :lastName, purpose = :purpose, time_in = :timeIn , date = :date';
        $stmt = $this->connection->prepare($query);

        $stmt->bindParam(':givenName', $this->givenName);
        $stmt->bindParam(':lastName', $this->lastName);
        $stmt->bindParam(':purpose', $this->purpose);
        $stmt->bindParam(':timeIn',  $this->timeIn);
        $stmt->bindParam(':date', $this->date);

        if($stmt->execute()) {
           return $this->connection->lastInsertId();
        }

        printf("Error: %s.\n", $stmt->error);

        return false;
    }

    public function update()
    {   date_default_timezone_set("Asia/Manila");

        $query = 'UPDATE ' . $this->tableName . ' SET given_name = :givenName, 
          last_name = :lastName, purpose = :purpose WHERE dtr_id = :dtrId';

        $stmt = $this->connection->prepare($query);

        $stmt->bindParam(':purpose', $this->purpose);
        $stmt->bindParam(':givenName', $this->givenName);
        $stmt->bindParam(':lastName', $this->lastName);
        $stmt->bindParam(':dtrId', $this->dtrId);

        if($stmt->execute()) {
            return true;
        }

        printf("Error: %s.\n", $stmt->error);

        return false;
    }

    public function readByDateToday()
    {
        date_default_timezone_set("Asia/Manila");
        $dtr = 'SELECT * FROM '. $this->tableName;
        $dtr .= ' WHERE date ="'.date('Y-m-d').'"';
        $dtr .= ' ORDER BY 1 ASC';

        $statement = $this->connection->prepare($dtr);

        $statement->execute();

        return $statement;
    }

    public function timeOut()
    {
        date_default_timezone_set("Asia/Manila");
        $query = 'UPDATE ' . $this->tableName . ' SET time_out = "'. date("H:i:s").'" WHERE dtr_id = :id';
        $stmt = $this->connection->prepare($query);

        $stmt->bindParam(':id', $this->dtrId);

        if($stmt->execute()) {
            return true;
        }

        printf("Error: %s.\n", $stmt->error);

        return false;
    }
}